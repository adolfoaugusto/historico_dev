# Historico_Dev
Históricos de sites que trabalhei na renderização do wordpress, ou ate mesmo alterações no front-end(*).

2016
_Estágio Desenvolvimento Web - IndexDigital_
  * Nordestetv - Programação da Band (tribunadoceara.uol.com.br/nordestv/).
  * Hilpro Idiomas - hilpro.com.br (wordpress).
  * Carmel Cumbuco - carmelcumbuco.com.br (wordpress)
  * Granspa - (* Front-end - pequenas alterações).
  * Tribuna do Ceará - (* Front-end - pequenas alterações).
  * Provedel - provedel.com.br (wordpress).
  * Agências Peixoto - agenciaspeixoto.com.br (php5).
  * Odonto System - sorria-club(sorriaclube.com.br).
  * BikeEx - Renderizado. (Site descontinuado).

2017
_Full Stack Web Developer_
  * Projesa Engenharia (http://projesa.com.br/ - wordpress).
  * New Masters Academy (Wordpress - new layout - http://www.newmastersacademy.org/).

